<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="./style.css">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
   <title>CSS Documentation</title>
</head>
<body>
   <div class="wrapper">
      <div class="nav-wrapper">
      <div class="navigation">
         <h2 class="nav-title">CSS Documentation</h2>
         <ul class="tab-list">
            <li class="tab-item"><a href="#introduction">Introduction</a></li>
            <li class="tab-item"><a href="#prerequisites">Prerequisites</a></li>
            <li class="tab-item"><a href="#modules">Modules</a></li>
            <li class="tab-item"><a href="#css-problems">Solving common CSS problems</a></li>
            <li class="tab-item"><a href="#working-css">Working with CSS</a></li>
            <li class="tab-item"><a href="#adding-css">Adding CSS</a></li>
            <li class="tab-item"><a href="#see-also">See also</a></li>
         </ul>
      </div>
      </div>
      <div class="content">
         <div class="tabcontent">
            <h1 class="title" id="introduction">Introduction</h1>
            <h2 class="subtitle">What is CSS?</h2>
            <ul class="list">
               <li class="list-item">CSS stands for Cascading Style Sheets</li>
               <li class="list-item">CSS describes how HTML elements are to be displayed on screen, paper, or in other media</li>
               <li class="list-item">CSS saves a lot of work. It can control the layout of multiple web pages all at once</li>
               <li class="list-item">External stylesheets are stored in CSS files</li>
            </ul>
         </div>
         <div class="tabcontent" id="prerequisites">
            <h1 class="title">Prerequisites</h1>
            <p class="paragraf">You should learn the basics of HTML before attempting any CSS. We recommend that you work through our Introduction to HTML module first.</p>
            <p class="paragraf">Once you understand the fundamentals of HTML, we recommend that you learn further HTML and CSS at the same time, moving back and forth between the two topics. This is because HTML is far more interesting and much more fun to learn when you apply CSS, and you can't learn CSS without knowing HTML.</p>
            <p class="paragraf">Before starting this topic, you should also be familiar with using computers and using the web passively (i.e., just looking at it, consuming the content). You should have a basic work environment set up, as detailed in Installing basic software, and understand how to create and manage files, as detailed in Dealing with files — both of which are parts of our Getting started with the web complete beginner's module.</p>
            <p class="paragraf">It is also recommended that you work through Getting started with the web before proceeding with this topic, especially if you are completely new to web development. However, much of what is covered in its CSS basics article is also covered in our CSS first steps module, albeit in a lot more detail.</p>
         </div>
         <div class="tabcontent">
            <h1 class="title" id="modules">Modules</h1>
            <dl>
               <dt>CSS first steps</dt>
               <dd>CSS (Cascading Style Sheets) is used to style and layout web pages — for example, to alter the font, color, size, and spacing of your content, split it into multiple columns, or add animations and other decorative features. This module provides a gentle beginning to your path towards CSS mastery with the basics of how it works, what the syntax looks like, and how you can start using it to add styling to HTML.</dd>
               <dt>CSS building blocks</dt>
               <dd>This module carries on where CSS first steps left off — now you've gained familiarity with the language and its syntax, and got some basic experience with using it, its time to dive a bit deeper. This module looks at the cascade and inheritance, all the selector types we have available, units, sizing, styling backgrounds and borders, debugging, and lots more. The aim here is to provide you with a toolkit for writing competent CSS and help you understand all the essential theory, before moving on to more specific disciplines like text styling and CSS layout.</dd>
               <dt>CSS styling text</dt>
               <dd>With the basics of the CSS language covered, the next CSS topic for you to concentrate on is styling text — one of the most common things you'll do with CSS. Here we look at text styling fundamentals, including setting font, boldness, italics, line and letter spacing, drop shadows, and other text features. We round off the module by looking at applying custom fonts to your page, and styling lists and links.</dd>
               <dt>CSS layout</dt>
               <dd>At this point, we've already looked at CSS fundamentals, how to style text, and how to style and manipulate the boxes that your content sits inside. Now it's time to look at how to place your boxes in the right place with respect to the viewport, and one another. We have covered the necessary prerequisites so we can now dive deep into CSS layout, looking at different display settings, modern layout tools like flexbox, CSS grid, and positioning, and some of the legacy techniques you might still want to know about.</dd>
            </dl>
         </div>
         <div class="tabcontent">
            <h1 class="title" id="css-problems">Solving common CSS problems</h1>
            <p class="paragraf">Use CSS to solve common problems provides links to sections of content explaining how to use CSS to solve very common problems when creating a web page.</p>
            <p class="paragraf">From the beginning, you'll primarily apply colors to HTML elements and their backgrounds; change the size, shape, and position of elements; and add and define borders on elements. But there's not much you can't do once you have a solid understanding of even the basics of CSS. One of the best things about learning CSS is that once you know the fundamentals, usually you have a pretty good feel for what can and can't be done, even if you don't know how to do it yet!</p>
         </div>
         <div class="tabcontent">
            <h1 class="title" id="working-css">Working with CSS</h1>
            <p class="paragraf">It is a good idea to have all of the common styling first in the stylesheet. This means all of the styles which will generally apply unless you do something special with that element. You will typically have rules set up for:</p>
            <ul class="list">
               <li class="list-item"><span>body</span></li>
               <li class="list-item"><span>p</span></li>
               <li class="list-item"><span>h1</span>, <span>h2</span>, <span>h3</span>, <span>h4</span>, <span>h5</span></li>
               <li class="list-item"><span>ul</span> and <span>ol</span></li>
               <li class="list-item">The table properties</li>
               <li class="list-item">Links</li>
            </ul>
         </div>
         <div class="tabcontent">
            <h1 class="title" id="adding-css">Adding CSS</h1>
            <p class="paragraf">The CSS we create will act as a style sheet for our web pages. This is what will control the type, color, layout and even interactive pieces. In order for our HTML pages to make use of the CSS rules, we need to make sure that our HTML page references, or attaches, them in some way.</p>
            <p class="paragraf">There are three common ways to attach your stylesheets:</p>
            <ul class="list">
               <li class="list-item">External</li>
               <li class="list-item">Embedded / Internal</li>
               <li class="list-item">Inline</li>
            </ul>
            <h4 class="subtitle">1) Inline CSS</h4>
            <p class="paragraf">Inline CSS is used to apply CSS on a single line or element.</p>
            <p class="paragraf">For example:</p>
               <code>
                  <span>&lt;p style="color:blue">Hello CSS/p&gt;</span>
               </code>
            <h4 class="subtitle">2) Internal CSS</h4>
            <p class="paragraf">Internal CSS is used to apply CSS on a single document or page. It can affect all the elements of the page. It is written inside the style tag within head section of html.</p>
            <p class="paragraf">For example:</p>
               <code>
               <span>&lt;style&gt;<br>p{color:blue}<br>&lt;style&gt;</span>
               </code>
               <h4 class="subtitle">3) External CSS</h4>
               <p class="paragraf">External CSS is used to apply CSS on multiple pages or all pages. Here, we write all the CSS code in a css file. Its extension must be .css for example style.css.</p>
               <p class="paragraf">For example:</p>
               <code>
                  <span>&lt;link rel="stylesheet" type="text/css" href="style.css"&gt;</span>
               </code>
         </div>
         <div class="tabcontent">
            <h1 class="title" id="see-also">See also</h1>
            <dl>
               <dt>CSS on MDN</dt>
               <dd>The main entry point for CSS documentation on MDN, where you'll find detailed reference documentation for all features of the CSS language. Want to know all the values a property can take? This is a good place to go.</dd>
            </dl>
         </div>
   </div>
   </div>
</body>
</html>